import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.*;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class MusicDownloader {
    public static int download(Dataset dataset, String path, View view) {
        String fileName = getNameFile(dataset);

        try {
            URL hp = new URL(dataset.getUrl());
            URLConnection hpCon = hp.openConnection();
            long len = hpCon.getContentLengthLong();

            long tempLen = 0;
            int c;
            boolean ten = false, forty = false, eighty = false;
            if (len == -1) {
                view.info.append("Content length unavailable\n");
                System.out.println("Content length unavailable");
            }

            if (len != 0) {
                view.info.append("\t" + fileName);
                System.out.print("\t" + fileName);
                view.info.append(", size: " + len + " bytes, ");
                System.out.print(", size: " + len + " bytes, ");
                InputStream input = hpCon.getInputStream();
                try (OutputStream fout = new BufferedOutputStream(Files.newOutputStream(Paths.get(path + fileName)))) {
                    while (((c = input.read()) != -1)) {
                        fout.write(c);
                        tempLen++;
                        if ((float) tempLen / (float) len > 0.100 && (float) tempLen / (float) len < 0.101 && !ten) {
                            view.info.append("10%...");
                            System.out.print("10%...");
                            ten = !ten;
                        }
                        if ((float) tempLen / (float) len > 0.400 && (float) tempLen / (float) len < 0.401 && !forty) {
                            view.info.append("40%...");
                            System.out.print("40%...");
                            forty = !forty;
                        }
                        if ((float) tempLen / (float) len > 0.800 && (float) tempLen / (float) len < 0.801 && !eighty) {
                            view.info.append("80%...");
                            System.out.print("80%...");
                            eighty = !eighty;
                        }
                    }
                } catch (InvalidPathException e) {
                    view.info.append("Path error " + e + "\n");
                    System.out.println("Path error " + e);
                } catch (IOException e) {
                    view.info.append("I/O error " + e + "\n");
                    System.out.println("I/O error " + e);
                }
                input.close();
            } else {
                view.info.append("No content available\n");
                System.out.println("No content available");
            }
            if (tempLen == len) {
                view.info.append("100%, успешно \n");
                System.out.println("100%, успешно ");
                return 0;
            } else {
                view.info.append(", ошибка\n");
                System.out.println(", ошибка");
                return 1;
            }
        } catch (MalformedURLException e) {
            view.info.append("Ошибка URL, возможно, файл был удалён правообладателем: " + fileName + "\n");
            System.out.println("Ошибка URL, возможно, файл был удалён правообладателем: " + fileName);
            return 1;
        } catch (IOException e) {
            view.info.append("Ошибка I/O: " + e + "\n");
            System.out.println("Ошибка I/O: " + e);
            return 1;
        }
    }

    public static String getNameFile(Dataset dataset) {
        String fileName = dataset.getArtist() + " - " + dataset.getTitle();
        fileName = fileName.replaceAll("\\/|\\?|\\*|\\:|\\<|\\>|\\|", "");
        fileName = fileName.replace('\"', ' ');
        if (fileName.length() > 250) {
            fileName = fileName.substring(0, 250);
        }
        fileName = fileName + ".mp3";
        return fileName;
    }

    public static boolean checkExist(Dataset dataset, Path dirPath) {
        try (DirectoryStream<Path> dirstrm = Files.newDirectoryStream(dirPath)) {
            for (Path entry : dirstrm) {
                if (entry.getFileName().toString().equals(getNameFile(dataset))) {
                    return true;
                }
            }
        } catch (InvalidPathException e) {
            System.out.println("Path error: " + e);
        } catch (NotDirectoryException e) {
            System.out.println(dirPath + " is not a directory: " + e);
        } catch (IOException e) {
            System.out.println("I/O error: " + e);
        }
        return false;
    }
}
