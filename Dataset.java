import java.util.HashMap;
import java.util.Map;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class Dataset {
    private String id;
    private String owner_id;
    private String artist;
    private String title;
    private String duration;
    private String date;
    private String url;
    private String lyrics_id;
    private String genre_id;

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public String getDuration() {
        return duration;
    }

    public String getDate() {
        return date;
    }

    public String getUrl() {
        return url;
    }

    public String getLyrics_id() {
        return lyrics_id;
    }

    public String getGenre_id() {
        return genre_id;
    }

    private Map<String , Object> otherProperties = new HashMap<String , Object>();

    public String getId() {
        return id;
    }

    public String getOwner_id() {
        return owner_id;
    }


    public Object get(String name) {
        return otherProperties.get(name);
    }

}
