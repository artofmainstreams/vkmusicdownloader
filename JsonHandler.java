import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class JsonHandler implements Runnable {
    Thread t;
    private String address;
    private String filePath;
    private View view;

    public JsonHandler(String address, String filePath, View view) {
        this.address = address;
        this.filePath = filePath;
        this.view = view;
        t = new Thread(this, "Мой поток"); //first arg: Runnable, second arg: nameThread
        t.start(); //запуск потока!
    }

    @Override
    public void run() {
        if (!filePath.endsWith("/")) {
            filePath += "/";
        }
        String json = JsonRequest.load(address);
        json = json.substring(json.indexOf("["), json.length() - 2);
        Gson gson = new Gson();
        // create the type for the collection. In this case define that the collection is of type Dataset
        Type datasetListType = new TypeToken<Collection<Dataset>>() {}.getType();
        List<Dataset> datasets = gson.fromJson(json, datasetListType);
        int count = 1;
        int errors = 0;

        count = 1;


        try {
            if (!Files.exists(Paths.get(filePath))) {
                Files.createDirectory(Paths.get(filePath));
            }
        } catch (IOException e) {
            view.info.append("Ошибка создания директории (возможно, она уже существует)\n");
            System.out.println("Ошибка создания директории (возможно, она уже существует)");
        }

        //проверяем на существование
        Path path = Paths.get(filePath);
        Iterator<Dataset> iter = datasets.iterator();
        while(iter.hasNext()){
            Dataset dataset = iter.next();
            if(MusicDownloader.checkExist(dataset, path)) {
                iter.remove();
            }
        }
        //-

        for (Dataset dataset : datasets) {
            view.info.append("(" + count + "/" + datasets.size() + ") " + "[" + errors + "] ");
            System.out.print("(" + count + "/" + datasets.size() + ") " + "[" + errors + "] ");
            errors += MusicDownloader.download(dataset, filePath, view);
            count++;
        }
        view.info.append("Успешно загружено: " + (count - errors - 1) + "\n");
        System.out.println("Успешно загружено: " + (count - errors - 1));
    }
}
