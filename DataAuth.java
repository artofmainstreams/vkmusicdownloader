/**
 * Created by artofmainstreams on 14.10.2016.
 */
public class DataAuth {
    private String access_token;
    private String expires_in;
    private String user_id;

    public String getAccess_token() {
        return access_token;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public String getUser_id() {
        return user_id;
    }
}
