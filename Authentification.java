import com.google.gson.Gson;

/**
 * Created by artofmainstreams on 14.10.2016.
 */
public class Authentification {
    private static DataAuth getToken(String login, String password) {
        String address = "https://oauth.vk.com/token?client_secret=VeWdmVclDCtn6ihuP1nt&client_id=3140623&username=" + login + "&password=" + password + "&grant_type=password";
        String request = JsonRequest.load(address);
        Gson gson = new Gson();
        // create the type for the collection. In this case define that the collection is of type Dataset
        return gson.fromJson(request, DataAuth.class);
    }

    public static String getLink(String login, char[] pass) {
        String password = new String(pass);
        return "https://api.vk.com/method/audio.get?owner_id=" + getToken(login, password).getUser_id() + "&need_user=0&count=5000&offset=0&access_token=" + getToken(login, password).getAccess_token() + "&v=5.58";

    }
}

