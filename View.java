import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by artofmainstreams on 14.10.2016.
 */
public class View {
    JTextArea info;

    View() {
        JFrame jFrame = new JFrame("VkMusicDownloader v0.4");
        jFrame.setLayout(new FlowLayout());
        jFrame.setSize(1300, 600);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton buttonGetMusic = new JButton("Get music");
        JLabel labelLogin = new JLabel("e-mail");
        JLabel labelPassword = new JLabel("password");
        JLabel labelPath = new JLabel("path");
        JTextField textLogin = new JTextField(30);
        JPasswordField textPassword = new JPasswordField(30);
        JTextField textPath = new JTextField(30);
        info = new JTextArea(30, 110);
        info.setEditable(false);
        info.append("VkMusicDownloader v0.4 by Andrei Khromov\n");
        JScrollPane scroll = new JScrollPane(info);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        buttonGetMusic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {new
                JsonHandler(Authentification.getLink(textLogin.getText(),
                        textPassword.getPassword()), textPath.getText(), View.this);
            }
        });

        jFrame.add(labelLogin);
        jFrame.add(textLogin);
        jFrame.add(labelPassword);
        jFrame.add(textPassword);
        jFrame.add(labelPath);
        jFrame.add(textPath);
        jFrame.add(buttonGetMusic);
        jFrame.add(scroll);


        jFrame.setVisible(true);
    }
}
