import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class JsonRequest {
    public static String load(String address) {
        URL url = null;
        URLConnection urlCon = null;
        StringBuilder jsonAnswer = new StringBuilder();
        String answer = null;
        try {
            url = new URL(address);
            urlCon = url.openConnection();
            if (urlCon.getContentLengthLong() != 0) {
                String line;
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    jsonAnswer.append(line);
                }
            } else {
                System.out.println("No content available");
            }
            answer = jsonAnswer.toString();
        } catch (MalformedURLException e) {
            System.out.println("MalformedURLException " + e);
        } catch (IOException e) {
            System.out.println("IOException " + e);
        }
        return answer;
    }
}
